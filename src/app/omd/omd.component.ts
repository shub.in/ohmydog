import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { OmdService } from './omd.service';

@Component({
  selector: 'app-omd',
  templateUrl: './omd.component.html',
  styleUrls: ['./omd.component.scss']
})
export class OmdComponent implements OnInit {
  selected = '';
  breeds = [];
  is_loading = true;
  picture = '';

  constructor(
    private omdService: OmdService,
    private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit(): void {
    this.is_loading = true;
    this.selected = this.route.snapshot.paramMap.get('breed');
    this.omdService.getList().subscribe((data) => {
      if (data['status'] === 'success') {
        for (const item in data['message']) {
          this.breeds.push(item);
        }
      }
    });
    if (this.selected == null) {
      this.is_loading = false;
    }
    this.get_image();
  }

  get_image() {
    this.is_loading = true;
    this.omdService.getItem(this.selected).subscribe((data) => {
      if (data['status'] === 'success') {
        this.picture = data['message'];
        this.is_loading = false;
      }
    });
  }

  onSelect() {
    this.router.navigate([this.selected]);
    this.get_image();
  }
}
