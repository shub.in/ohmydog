import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class OmdService {

  constructor(private http: HttpClient) {}

  getList(): Observable<any> {
    const url = `https://dog.ceo/api/breeds/list/all`;
    return this.http.get<any>(url);
  }

  getItem(breed: string): Observable<any> {
    const url = `https://dog.ceo/api/breed/${breed}/images/random`;
    return this.http.get<any>(url);
  }
}
