import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule, MatButtonModule, MatSelectModule, MatFormFieldModule,
  MatProgressSpinnerModule, MatIconModule } from '@angular/material';

import { AppComponent } from './app.component';
import { OmdService } from './omd/omd.service';
import { OmdComponent } from './omd/omd.component';


const routes: Routes = [
  { path: '', component: OmdComponent },
  { path: ':breed', component: OmdComponent },
  { path: '**', redirectTo: '/', pathMatch: 'full' },
];


@NgModule({
  declarations: [
    AppComponent,
    OmdComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatSelectModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
  providers: [OmdService],
  bootstrap: [AppComponent]
})
export class AppModule { }
